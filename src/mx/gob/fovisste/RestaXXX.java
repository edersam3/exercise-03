package mx.gob.fovisste;

import java.math.BigDecimal;

/**
 * @author Eder Sam
 *
 */
public class RestaXXX {

	public static void main(String[] args) {

		if (args == null || args.length == 0) {
			System.out.println("Es necesario introducir 3 valores, ejemplo 'RestaXXX 2 15.2 15.0'.");
			System.exit(0);
		}

		int numberOfDecimals = 0;
		double d1 = 0d;
		double d2 = 0d;

		try {
			numberOfDecimals = Integer.parseInt(args[0]);
		} catch (Exception e) {
			System.out.println("Numero de decimales incorrecto.");
			System.exit(0);
		}

		try {
			d1 = Double.parseDouble(args[1]);
		} catch (Exception e) {
			System.out.println("Valor 1 incorrecto.");
			System.exit(0);
		}

		try {
			d2 = Double.parseDouble(args[2]);
		} catch (Exception e) {
			System.out.println("Valor 2 incorrecto.");
			System.exit(0);
		}

		double d3 = d1 - d2;
		System.out.println("Valor no redondeado = " + d3);

		double roundedValue = new RestaXXX().roundsToDecimals(d3, numberOfDecimals);
		System.out.println("Valor redondeado = " + roundedValue);
	}

	public double roundsToDecimals(double unroundedValue, int numberOfDecimals) {
		BigDecimal bigDecimal = new BigDecimal(unroundedValue);
		BigDecimal roundedWithScale = bigDecimal.setScale(numberOfDecimals, BigDecimal.ROUND_HALF_UP);
		return roundedWithScale.doubleValue();
	}

}
