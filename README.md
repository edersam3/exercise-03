3. Copia la siguiente clase en un archivo nuevo

	import java.math.*;
	public class RestaXXX
	{
		public static void main (String[] args)
		{
			double d1 = Double.parseDouble( args[0] );
			double d2 = Double.parseDouble( args[1] );
			double d3 = d1 - d2;
			System.out.println(d3);
		}
	}

Y compílala y ejecútala de tal manera que comprueba que java no maneja exactitud en los decimales.

java RestaXXX 15.2 15.0
0.1999999999999993

Crea un método cuya firma sea:

	public double redondeaADecimales(double valorSinRedondear, int 	numeroDeDecimales)

Que reciba un valor como los mostrados arriba y devuelva el numérico redondeado a solamente los
decimales definidos en el segundo parámetro, de tal manera que ahora la clase reciba el número de decimales y los operandos desde la línea de comandos y los resultados anteriores sean:

java RestaXXX 2 15.2 15.0
0.20